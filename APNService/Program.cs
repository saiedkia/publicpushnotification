﻿using NLog;
using PushSharp.Apple;
using PushSharp.Google;
using System;
using System.Linq;
using Topshelf;

namespace PushNotificationService
{
    class Program
    {
        public static ApnsServiceBroker __ApnBroker;
        public static FcmServiceBroker __FcmBroker;
        public static Logger _logger;

        static void Main(string[] args)
        {
            _logger = LogManager.GetCurrentClassLogger();
           
           
            
            string assemblyPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            int index = assemblyPath.Reverse().ToList().IndexOf('\\');
            string[] configs = System.IO.File.ReadLines(assemblyPath.Substring(0, assemblyPath.Length - index) + "config.txt").ToArray();

            string hostUrl = configs[0];
            string fcmToken = configs[1];
            string apnPassword = configs[2];


            TopshelfExitCode serviceExitCode = HostFactory.Run(configurator =>
            {
                configurator.Service<PushService>(factory =>
               {
                   factory.ConstructUsing(f => new PushService());
                   factory.WhenStarted(x => x.Start(hostUrl, apnPassword, fcmToken));
                   factory.WhenStopped(x => x.Stop());
               });

                configurator.RunAsLocalSystem();
                configurator.SetDescription("Push notification service");
                configurator.SetDisplayName("PushNotificationService");
                configurator.SetServiceName("PushNotificationService");

            });

            var exitCode = (int)Convert.ChangeType(serviceExitCode, serviceExitCode.GetTypeCode());
            Environment.ExitCode = exitCode;

        }
    }
}
