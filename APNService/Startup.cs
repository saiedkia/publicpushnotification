﻿using Owin;
using System.Web.Http;

namespace PushNotificationService
{
    public class Startup
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            HttpConfiguration config = new HttpConfiguration();
            config.Routes.MapHttpRoute(
                name: "default",
                routeTemplate: "{controller}/{method}"
            );

            appBuilder.UseWebApi(config);
        }
    }
}
