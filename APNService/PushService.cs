﻿using Microsoft.Owin.Hosting;
using PushSharp.Apple;
using PushSharp.Google;
using System;

namespace PushNotificationService
{
    public class PushService
    {
        IDisposable _service;

        public void Start(string url, string apnPassword, string fcmToken)
        {
            _service = WebApp.Start<Startup>(url);

            APN(apnPassword);
            FCM(fcmToken);
        }

        private static void APN(string apnPassword)
        {
            ApnsConfiguration apnConfig = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Sandbox, "Certificate_apn.p12", apnPassword);
            ApnsServiceBroker apnBroker = new ApnsServiceBroker(apnConfig);
            apnBroker.OnNotificationFailed += (notification, exp) =>
            {

                Console.WriteLine(exp);
                Program._logger.Error("APN: " + exp);
            };

            apnBroker.OnNotificationSucceeded += (notification) =>
            {
                Console.WriteLine(notification);
                Program._logger.Info("APN: " + notification);

            };

            apnBroker.Start();
            Program.__ApnBroker = apnBroker;
        }

        private static void FCM(string fcmToken)
        {
            FcmConfiguration fcmConfiguration = new FcmConfiguration(fcmToken);
            FcmServiceBroker fcmBroker = new FcmServiceBroker(fcmConfiguration);
            fcmBroker.OnNotificationFailed += (notification, exp) =>
            {
                Console.WriteLine(exp);
                Program._logger.Error("FCM: " + exp);
            };

            fcmBroker.OnNotificationSucceeded += (notification) =>
            {
                Console.WriteLine(notification);
                Program._logger.Info("FCM: " + notification);
            };


            fcmBroker.Start();
            Program.__FcmBroker = fcmBroker;
        }

        public void Stop()
        {
            _service.Dispose();
        }
    }
}
