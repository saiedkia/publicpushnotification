﻿using Newtonsoft.Json.Linq;
using PushSharp.Apple;
using PushSharp.Google;
using System;
using System.Web.Http;

namespace PushNotificationService
{
    public class PushController : ApiController
    {
        [HttpPost]
        public bool Send([FromBody] Notification notification)
        {
            try
            {
                if (notification.MessageType == MessageType.APN)
                    return SendAPN(notification.Token, notification.Message, notification);
                else
                    return SendFCM(notification.Token, notification.Message, notification);
            }
            catch (Exception exp)
            {
                Program._logger.Error(notification + "\n" + exp);
            }


            return false;
        }


        private bool SendAPN(string deviceId, string message, Notification notification)
        {
            ApnsNotification apnNotification = new ApnsNotification(deviceId);
            if (message.Contains("payload"))
            {
                string _message = @"{'aps':
		            {
                        'content-available': 1,
                        'type' : 1,
			            'payload': {
                                    'message' : {message}
                                   }
                    }}";

                string payload = "\"" + message + "\"";
                _message = _message.Replace("{message}", payload);

                apnNotification.Payload = JObject.Parse(_message);

                Program.__ApnBroker.QueueNotification(apnNotification);
            }
            else
            {
                string _message = "{\"aps\":{\"badge\":-1,\"alert\":\"" + message + "\"}}";
                apnNotification.Payload = JObject.Parse(_message);

                Program.__ApnBroker.QueueNotification(apnNotification);

            }


            return true;
        }

        private bool SendFCM(string key, string message, Notification notification)
        {
            JObject jNotification = new JObject();
            jNotification.Add("title", "push title");
            jNotification.Add("body", message);
            jNotification.Add("sound", "default");

            //if (true /*some condition*/)
            //{
            //    jNotification.Add("click_action", "HomeActivity");
            //    jNotification.Add("data", "{'extra':'inbox'}");
            //}

            JObject jData = new JObject();
            jData.Add("payload", "{ 'message' : {message}}".Replace("{message}", "\"" + message + "\""));

            FcmNotification n = new FcmNotification()
            {

                Notification = jNotification,
                Priority = FcmNotificationPriority.Normal,
                Data = jData,
                To = key
            };

            Program.__FcmBroker.QueueNotification(n);

            return true;
        }
    }
}
