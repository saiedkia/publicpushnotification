﻿namespace PushNotificationService
{
    public class Notification
    {
        public string Message { get; set; }
        public string Token { get; set; } // receiver token/device id
        public MessageType MessageType { get; set; }
    }

    public enum MessageType
    {
        FCM = 1,
        APN
    }
}